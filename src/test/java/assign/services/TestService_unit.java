package assign.services;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.validateMockitoUsage;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.junit.Test;

public class TestService_unit {

	ServiceLink gs = new ServiceLink();
	@Test
	public void test_getUrl_1() {
		String[] url1 = {"meetings", "barbican","2013"};
		assertEquals("http://eavesdrop.openstack.org/meetings/barbican/2013", gs.getURL(url1));
	}
	@Test
	public void test_getUrl_2() {
		String[] url2 = {"irclogs", "%23heat",""};
		assertEquals("http://eavesdrop.openstack.org/irclogs/%23heat/", gs.getURL(url2));
	}
	

	

}
