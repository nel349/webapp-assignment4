package assign.resources;

import java.util.ArrayList;
import java.util.List;

import assign.resources.*;
import assign.services.Project;
import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class TestResource_unit {
	
	RESTEasyHelloWorldService as = new RESTEasyHelloWorldService();
	@Test
	public void test_convertToProjectList_1() {
	
		List<String> a = new ArrayList<String>();
		a.add("test1");
		a.add("test2");
		a.add("test3");
		
		List<Project> expected = new ArrayList<Project>();
		expected.add(new Project("test1"));
		expected.add(new Project("test2"));
		expected.add(new Project("test3"));
		
		
		List<Project> actual = as.convertToProjectList(a);
		assert(expected == actual);
	}
	@Test
	public void test_convertToLinkList_1() {
		List<String> a = new ArrayList<String>();
		a.add("test1");
		a.add("test2");
		a.add("test3");
		
		List<Project> expected = new ArrayList<Project>();
		expected.add(new Project("test1"));
		expected.add(new Project("test2"));
		expected.add(new Project("test3"));
		
		
		List<Project> actual = as.convertToProjectList(a);
		assert(expected == actual);
	}

}
