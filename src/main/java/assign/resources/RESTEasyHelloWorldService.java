package assign.resources;


import java.util.ArrayList;
import java.util.List;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.xml.bind.annotation.XmlAttribute;

import org.jboss.resteasy.annotations.providers.jaxb.Wrapped;

import assign.services.Link;
import assign.services.Project;
import assign.services.ServiceLink;

@Path("/")
public class RESTEasyHelloWorldService {
	ServiceLink ads = new ServiceLink();
	
	

	@GET
	@Path("/projects/{project}/meetings/{year}")
	@Produces("application/xml")
	@Wrapped(element = "project")
	@XmlAttribute(name = "project")
	public List<Link> get_all_files_from_project_and_year_in_meetings( @PathParam("project") String project, @PathParam("year") String year) {
		String[] link1 = {"meetings", project, year};
		
	    List<String> a = ads.getData(link1, true);
	    
	    List<Link> result = this.convertToLinkList(a);
	    
		return result;
	}


	@GET
	@Path("/projects/{project}/irclogs")
	@Produces("application/xml")
	@Wrapped(element = "project")
	@XmlAttribute(name = "project")
	public List<Link> get_all_files_from_project_and_year_in_irclogs( @PathParam("project") String project) {
		String[] link1 = {"irclogs", project, ""};
		
	    List<String> a = ads.getData(link1, true);
	    
	    List<Link> result = this.convertToLinkList(a);
	    
		return result;
	}


	@GET
	@Path("/projects")//without / at the end
	@Produces("application/xml")
	@Wrapped(element = "projects")
	public List<Project> get_all_projects() {
		String[] link1 = {"meetings","", ""};
		String[] link2 = {"irclogs","", ""};
	    List<String> a = ads.getData(link1, false);
	    List<String> b = ads.getData(link2, false);
	    List<Project> result = this.convertToProjectList(a);
	    result.addAll(this.convertToProjectList(b));
	    
		return result;
	}


	public List<Project> convertToProjectList(List<String> a){
		List<Project> result = new ArrayList<Project>();

		for(String x : a){
			result.add(new Project(x));
		}

		return result;
	}
	
	public List<Link> convertToLinkList(List<String> a){
		List<Link> result = new ArrayList<Link>();

		for(String x : a){
			result.add(new Link(x));
		}

		return result;
	}
	


}