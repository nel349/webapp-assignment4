package assign.services;

import javax.xml.bind.annotation.*;


@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement(name = "project")
public class Project {
	
    @XmlValue
    private String name;
    

    public Project(){
    	
    }
    
    public Project(String name)
    {
       
        this.name = name;
        

    }
    
    public boolean equals(Project other)
    {
       if (this.name.equals(other.name) )
       {
          return true;
       }
       return false;

    }
}
