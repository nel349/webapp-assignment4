package assign.services;

import javax.xml.bind.annotation.*;

@XmlAccessorType(XmlAccessType.FIELD)
@XmlRootElement

public class Link 
{
	
    @XmlValue
    private String link;


    public Link()
    {

    }

    public Link(String link)
    {

        this.link = link;
    }
}

   